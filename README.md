# Make a tree
Vanilla but Three JS

[online demo](https://jniac-playground.gitlab.io/easing/)

[animator.js](./public/src/animator/animator.js) is heavy, but not the rest (except three)
[math-utils.js](./public/src/math-utils.js) has useful tiny functions

[animation is here](https://gitlab.com/jniac-playground/easing/-/blob/master/public/src/main.js#L73-89)