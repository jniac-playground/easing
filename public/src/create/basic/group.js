import { DEBUG } from '../../config.js'
import * as THREE from '../../three/three.module.js'
import { arrange } from './arrange.js'

export const createGroup = ({
    
  // rien ici ! 
  ...props

} = {}) => {

  const group = new THREE.Group()
  
  arrange(group, props)

  if (DEBUG) {
    group.add(new THREE.AxesHelper())
  }

  return group
}
