import * as THREE from '../../three/three.module.js'
import { arrange } from './arrange.js'

const dummy = new THREE.Object3D()
const dummyColor = new THREE.Color()

// NOTE: THIS IS WIP

/**
 * Ajoute la méthode "addInstance" sur les objets THREE.InstancedMesh
 * @param {THREE.InstancedMesh} instanceMesh 
 */
export const initInstancedMesh = (instanceMesh) => {

  let index = 0

  instanceMesh.addInstance = ({

    color = 'white',
    ...props

  }) => {

    if (index === instanceMesh.count) {
      console.warn(`could not add instance`)
      return
    }

    arrange(dummy, props)
    dummy.updateMatrix()
    dummyColor.set(color)

		instancedMesh.setMatrixAt(index, dummy.matrix)
		instancedMesh.setColorAt(index, dummyColor)

    index += 1
  }
}
