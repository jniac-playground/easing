import * as THREE from './three/three.module.js'
import { createLights } from './create/lights.js'
import { createSky } from './create/sky.js'
import { camera, canvas, scene } from './stage/stage.js'
import { OrbitControls } from './three/from-examples/OrbitControls.js'
import { createMesh } from './create/basic/mesh.js'
import { createGroup } from './create/basic/group.js'
import { tween } from './animator/animator.js'
import { lerp, radians } from './utils.js'
import './UI.js'
import { easeInout, pcurve } from './math-utils.js'

camera.position.set(0, 0, 10)

const controls = new OrbitControls(camera, canvas)
controls.target.set(0, 0, 0)
controls.update()

createLights()
createSky({ color:'#385EBE' })

const createGrid = ({
	sizeX = 6,
	sizeY = 6,
} = {}) => {

	const parent = createGroup()
	const geometry = new THREE.PlaneGeometry()
	const frontMaterial = new THREE.MeshPhysicalMaterial({
		color: '#fc0',
	})
	const backMaterial = new THREE.MeshPhysicalMaterial({
		color: '#63f',
	})

	for (let x = 0; x < sizeX; x++) {
		for (let y = 0; y < sizeY; y++) {
			const plane = createGroup({ 
				x: x - (sizeX - 1) / 2,
				y: y - (sizeY - 1) / 2,
				userData: { x, y, index:sizeX * y + x },
				parent,
			})
			const props = {
				geometry,
				// castShadow: true,
				// receiveShadow: true,
				onPointerDown: () => flip(),
				parent: plane,
			}
			createMesh({
				material: frontMaterial,
				...props,
			})
			createMesh({
				material: backMaterial,
				rotationY: 180,
				...props,
			})
		}	
	}

	return parent
}

const gridProps = {
	sizeX: 7,
	sizeY: 7,
}

const planes = createGrid(gridProps)
planes.progress = 0

const flip = () => {
	const to = planes.progress > .5 ? 0 : 1
	tween(planes, 'progress', {
		to,
		duration: 4,
		onUpdate: () => {
			for (const plane of planes.children) {
				const { x, y } = plane.userData
				const i = (x + y) / (gridProps.sizeX + gridProps.sizeY - 2)
				const t = easeInout(planes.progress, 3, lerp(.2, .8, i))
				plane.scale.setScalar(1 - .5 * pcurve(t, 2, 2))
				plane.rotation.y = radians(180 * t)
				plane.rotation.z = radians(90 * t)
			}
		},
	})
}

flip()


// debug
const debugMain = async () => {
	const stage = await import('./stage/stage.js')
	Object.assign(window, { 
		camera,
		controls,
		stage,
		controls,
		THREE,
	})
}
debugMain()
