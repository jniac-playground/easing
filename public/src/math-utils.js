export const clamp01 = x => x < 0 ? 0 : x > 1 ? 1 : x

export const clamp = (x, min = 0, max = 1) => x < min ? min : x > max ? max : x

export const lerp = (a, b, x) => a + (b - a) * clamp01(x)

export const inverseLerp = (x, min, max) => clamp01((x - min) / (max - min))

export const map = (x, inMin, inMax, outMin, outMax) => lerp(outMin, outMax, inverseLerp(x, inMin, inMax))

export const easeIn = (x, power = 3) => clamp01(x) ** power

export const easeOut = (x, power = 3) => 1 - (1 - clamp01(x)) ** power

// https://www.desmos.com/calculator/ynzjoms155
export const easeInout = (x, power = 3, inflexion = 0.33) => {
  x = clamp01(x)
  if (x <= inflexion) {
    return (inflexion ** (1 - power)) * (x ** power)
  }
  return 1 - ((1 - inflexion) ** (1 - power)) * ((1 - x) ** power)
}
export const easeInoutBind = (power = 3, inflexion = 0.33) => {
  const i1 = inflexion ** (1 - power)
  const i2 = (1 - inflexion) ** (1 - power)
  return x => {
    x = clamp01(x)
    if (x < inflexion) {
      return i1 * (x ** power)
    }
    return 1 - i2 * ((1 - x) ** power)
  }
}

// https://www.desmos.com/calculator/9h6o072i43 (from Inigo Quilez)
export const pcurve = (x, a = 3, b = 3) => {
  x = clamp01(x)
  const k = ((a + b) ** (a + b)) / ((a ** a) * (b ** b))
  return k * (x ** a) * ((1 - x) ** b)
}
export const pcurveBind = (a = 3, b = 3) => {
  const k = ((a + b) ** (a + b)) / ((a ** a) * (b ** b))
  return x => {
    x = clamp01(x)
    return k * (x ** a) * ((1 - x) ** b)
  }
}