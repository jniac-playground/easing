import * as THREE from '../three/three.module.js'

// https://threejs.org/docs/?q=raycas#api/en/core/Raycaster

const raycaster = new THREE.Raycaster()

export const initPointer = (canvas, scene, camera) => {
	
	const pointer = new THREE.Vector2()
	
	let targets = []
	const updatePointer = () => {
		raycaster.setFromCamera(pointer, camera)
		targets = raycaster.intersectObject(scene, true)
	}
	
	const onPointerMove = (event) => {
		pointer.x = (event.clientX / window.innerWidth) * 2 - 1
		pointer.y = - (event.clientY / window.innerHeight) * 2 + 1
	}
	const onPointerDown = () => {
		const [first] = targets
		first?.object?.onPointerDown?.(first)
	}

	canvas.addEventListener('pointermove', onPointerMove)
	canvas.addEventListener('pointerdown', onPointerDown)

	return { updatePointer }
}