
import * as THREE from '../three/three.module.js'
import { initPointer } from './pointer.js'
import * as loaders from './loaders.js'

export { loaders }

export const canvas = document.querySelector('canvas#three')

// Les valeurs par défaut ne font pas sens (une scène de 1 pixel par 1)
// La fonction 'updateSize' se charge de mettre à jour les valeurs en fonction 
// de la taille du navigateur
export let width = 1
export let height = 1
export let aspect = 1

export const scene = new THREE.Scene()
export const camera = new THREE.PerspectiveCamera(60, aspect)
camera.position.z = 7

export const renderer = new THREE.WebGLRenderer({ 
	canvas,
	antialias: true,
})

renderer.setPixelRatio(window.devicePixelRatio)

// activation des ombres portées
renderer.shadowMap.enabled = true
renderer.shadowMap.type = THREE.PCFShadowMap

const updateSize = () => {

	width = window.innerWidth
	height = window.innerHeight
	aspect = width / height

	camera.aspect = aspect
	camera.updateProjectionMatrix()

	renderer.setSize(width, height)
}

updateSize()

// On ajoute un écouteur (listener) sur l'évènement "resize"
// afin d'être "responsive" (de s'adapter au format de la fenêtre)
window.addEventListener('resize', () => updateSize())

const { updatePointer } = initPointer(canvas, scene, camera)

// Gestion de la pause automatique.
let pauseCountdown = 60 * 10
export const requestRender = (frameDuration = 60 * 10) => {
	pauseCountdown = Math.max(pauseCountdown, frameDuration)
}

export let frame = 0
const loop = () => {

	requestAnimationFrame(loop)

	if (pauseCountdown > 0) {
		scene.traverse(child => child.onFrame?.(child))
		updatePointer()
		renderer.render(scene, camera)
	}

	frame += 1
	pauseCountdown += -1
}

loop()

window.addEventListener('pointermove', () => requestRender())
window.addEventListener('pointerdown', () => requestRender())
window.addEventListener('pointerup', () => requestRender())
window.addEventListener('wheel', () => requestRender())
window.addEventListener('keydown', () => requestRender())
