import { resolveEase } from './ease.js'
/**
 * Double associates two keys (target, key) with one value :
 *
 *     const value = dm.get(target, key)
 *     dm.set(target, key, value)
 *
 * It's used here to bind a tween to a target (object) an a given property (string) :
 *
 *     new DoubleMap<object, string, Interval>()
 */
export class DoubleMap {
	constructor() {
		this.#map = new Map()
		this.#size = 0
	}
	#map
	#size
	get size() { return this.#size; }
	has(target, key) {
		return this.#map.get(target)?.has(key) ?? false
	}
	get(target, key) {
		return this.#map.get(target)?.get(key)
	}
	/**
	 * returns a clone Map if entry exists, or undefined otherwises.
	 * This is not optimal, and should be used only for debug purposes.
	 * For enumeration purposes prefer 'entries', 'getEntries'.
	 * @param target
	 * @returns
	 */
	getMap(target) {
		const map = this.#map.get(target)
		return map && new Map(map)
	}
	*entries() {
		for (const [target, map] of this.#map.entries()) {
			for (const [key, value] of map.entries()) {
				yield [target, key, value]
			}
		}
	}
	getEntries(target) {
		return this.#map.get(target)?.entries() ?? (function* () { })()
	}
	set(target, key, value) {
		const create = () => {
			const map = new Map()
			this.#map.set(target, map)
			return map
		}
		const map = this.#map.get(target) ?? create()
		if (map.has(key) === false) {
			this.#size++
		}
		return map.set(key, value)
	}
	delete(target, key) {
		const map = this.#map.get(target)
		if (map === undefined) {
			return false
		}
		const deleted = map.delete(key)
		if (deleted) {
			this.#size--
		}
		if (map.size === 0) {
			this.#map.delete(target)
		}
		return deleted
	}
	deleteAll(target) {
		const map = this.#map.get(target)
		if (map !== undefined) {
			this.#size -= map.size
			map.clear()
			this.#map.delete(target)
			return true
		}
		return false
	}
	clear() {
		for (const map of this.#map.values()) {
			this.#size -= map.size
			map.clear()
		}
		this.#map.clear()
	}
}
/**
 * Extends the concept of Map, for a given key, associate N values :
 *
 *     const callbacks = new MultipleMap<string, () => void>()
 *     callbacks.add('foo', () => console.log(`It's foo!`))
 *     callbacks.add('bar', () => console.log(`It's bar!`))
 *
 * Useful but... not used here! (could be used for ticker 'onComplete')
 * Kept as a memo.
 */
export class MultipleMap {
	constructor() {
		this.#map = new Map()
		this.#size = 0
	}
	#map
	#size
	get size() { return this.#size; }
	add(key, value) {
		const create = () => {
			const set = new Set()
			this.#map.set(key, set)
			return set
		}
		const set = this.#map.get(key) ?? create()
		const beforeSize = set.size
		set.add(value)
		this.#size += set.size - beforeSize
		return this
	}
	get(key) {
		const set = this.#map.get(key)
		return set && new Set(set)
	}
	delete(key, value) {
		const set = this.#map.get(key)
		if (set === undefined) {
			return false
		}
		if (set.delete(value)) {
			if (set.size === 0) {
				this.#map.delete(key)
			}
			this.#size--
			return true
		}
		return false
	}
	deleteAll(key) {
		const set = this.#map.get(key)
		if (set !== undefined) {
			this.#size += -set.size
			set.clear()
		}
		return this.#map.delete(key)
	}
	clear() {
		for (const set of this.#map.values()) {
			this.#size = -set.size
			set.clear()
		}
		this.#map.clear()
	}
}
const getRequestAnimationFrame = () => {
	const start = Date.now()
	return ((cb) => {
		setTimeout(() => cb(Date.now() - start), 16)
	})
}
const requestAnimationFrame = globalThis.requestAnimationFrame ?? getRequestAnimationFrame()
const MAX_DELTA_TIME = 1 / 10
const onTick = new Set()
const onCompleteMap = new Map()
const onCompleteAdd = (ticker, cb) => {
	const create = () => {
		const set = new Set()
		onCompleteMap.set(ticker, set)
		return set
	}
	(onCompleteMap.get(ticker) ?? create()).add(cb)
}
export let time = 0
export let frame = 0
let msOld = 0
const tick = (ms) => {
	const dt = Math.min((-msOld + (msOld = ms)) / 1000, MAX_DELTA_TIME)
	time += dt
	frame += 1
	const toRemove = new Set()
	for (const cb of onTick) {
		if (cb(dt)) {
			toRemove.add(cb)
		}
	}
	for (const cb of toRemove) {
		onTick.delete(cb)
		const onComplete = onCompleteMap.get(cb)
		onCompleteMap.delete(cb)
		if (onComplete !== undefined) {
			for (const cb of onComplete) {
				cb()
			}
		}
	}
	requestAnimationFrame(tick)
}
requestAnimationFrame(ms => {
	msOld = ms
	requestAnimationFrame(tick)
})
export const during = (duration, cb = () => { }, { delay = 0, startImmediate = false, } = {}) => {
	let time = -delay
	let timeOld = 0
	let complete = false
	let canceled = false
	const cancel = () => canceled = true
	const getTiming = () => {
		const progress = duration === 0 ? 1 : Math.max(time, 0) / duration
		return { time, timeOld, duration, progress, complete, canceled }
	}
	const then = (onfullfilled) => {
		if (complete === false) {
			onCompleteAdd(ticker, () => onfullfilled(getTiming()))
		}
		else {
			onfullfilled(getTiming())
		}
	}
	const interval = { cancel, then, getTiming, get timing() { return getTiming() } }
	const ticker = deltaTime => {
		timeOld = time
		time = Math.min(time + deltaTime, duration)
		const started = time > 0
		complete = canceled || time === duration
		if (started && canceled === false) {
			cb(interval)
		}
		return complete
	}
	if (startImmediate) {
		ticker(0)
		cb(interval)
	}
	onTick.add(ticker)
	return interval
}
const tweenIntervals = new DoubleMap()
const ensureKeys = (keys) => {
	const split = (str) => str.trim().split(/\s*,\s*/).filter(key => !!key)
	if (Array.isArray(keys)) {
		return keys.filter(key => !!key).map(split).flat()
	}
	return split(keys)
}
/**
 * Create a "Tween", a concept coming essentially from Greensock (TweenMax),
 * implemented here with subtle differences:
 *  - keys (eg: 'x,y,z') are given separately (no conflicts with API reserved words (ex: "from"))
 *  - Tween are not a class, but just a kind of struct, an object's wrapper that holds references to the intervals & useful functions.
 *  - The result could be awaited: it's an "thenable" object!
 *  - The tween could be easily canceled: `const { cancel } = tween(foo, 'x', { to:1 })`. Useful for canceling tween when the target is about to be destroyed (eg: a React component).
 *
 *
 * TODO: implement multiple from/to values, eg:
 *
 *     tween(foo, 'x,y,z', { to:[1,2,3] })
 *     tween(foo, 'x,y,z', { to:{ x:1, y:2, z:3 } }) // both?
 *
 * @param target The target of the animation! (any object, could be null, if so returns a zero-tween).
 * @param keys The keys associated to that object (could be multiple: 'x,y,z' or 'x'|'y'|'z').
 * @param props The properties of the tween (essentially "from", "to", "ease").
 * @returns a "Tween"
 */
export const tween = (target, keys, props = null) => {
	if (target === null || target === undefined) {
		return {
			intervals: [],
			cancel: () => { },
			then: onfullfilled => onfullfilled(),
		}
	}
	const intervals = []
	const ease = resolveEase(props?.ease ?? 'out3')
	for (const key of ensureKeys(keys)) {
		// cancel any existing intervals
		tweenIntervals.get(target, key)?.cancel()
		const { 
			from = Number(target[key] ?? 0), 
			to = Number(target[key] ?? 0), 
			duration = 1, 
			delay = 0, 
			onUpdate, 
			// 'startImmediate' is true by default of "from-tween":
			// The object is there, but should come from somewhere else
			// and we don't want to wait for the next tick/frame.
			startImmediate = !!props && 'from' in props 
		} = props ?? {}
		const interval = during(duration, interval => {
			const { progress, complete } = interval.getTiming()
			target[key] = from + (to - from) * ease(progress)
			onUpdate?.(interval)
			if (complete) {
				tweenIntervals.delete(target, key)
			}
		}, { delay, startImmediate })
		tweenIntervals.set(target, key, interval)
		intervals.push(interval)
	}
	const cancel = () => {
		for (const interval of intervals) {
			interval.cancel()
		}
	}
	const then = (onfullfilled) => Promise.all(intervals).then(() => onfullfilled())
	return {
		intervals,
		cancel,
		then,
	}
}
export const getTweensOf = (target, keys = '*') => {
	if (target === null || target === undefined) {
		return []
	}
	const intervals = []
	if (keys === '*') {
		for (const [, interval] of tweenIntervals.getEntries(target)) {
			intervals.push(interval)
		}
	}
	else {
		for (const key of ensureKeys(keys)) {
			const interval = tweenIntervals.get(target, key)
			if (interval) {
				intervals.push(interval)
			}
		}
	}
	return intervals
}
export const killTweensOf = (target, keys = '*') => {
	if (target === null || target === undefined) {
		return 0
	}
	let count = 0
	if (keys === '*') {
		for (const [, interval] of tweenIntervals.getEntries(target)) {
			interval.cancel()
			count++
		}
		tweenIntervals.deleteAll(target)
	}
	else {
		for (const key of ensureKeys(keys)) {
			const interval = tweenIntervals.get(target, key)
			if (interval) {
				interval.cancel()
				tweenIntervals.delete(target, key)
				count++
			}
		}
	}
	return count
}
export const animatorDebug = async () => {
	const Ease = await import('./Ease')
	const Animator = {
		tweenDoubleMap: tweenIntervals,
		during,
		tween,
		killTweensOf,
		Ease,
	}
	Object.assign(globalThis, { Animator })
}
