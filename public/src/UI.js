
let isFullscreen = false
const div = document.querySelector('div.fullscreen-button')
div.addEventListener('click', () => {
	if (isFullscreen === false) {
		document.body.requestFullscreen()
		isFullscreen = true
	} else {
		document.exitFullscreen()
		isFullscreen = false
	}
})
